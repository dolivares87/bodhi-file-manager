import gulp from 'gulp';
import eslint from 'gulp-eslint';
import babel from 'gulp-babel';
import sourcemaps from 'gulp-sourcemaps';
import runSequence from 'run-sequence';
import sloc from 'gulp-sloc';
import bumpVersion from 'gulp-bump';
import del from 'del';
import todo from 'gulp-todo';
import git from 'gulp-git-streamed-wrapper';
import fs from 'fs';

/* ************************************************************************* */
/* Helpers                                                                   */
/* ************************************************************************* */

/*
* Get any file synchronously
*/
function getFile(filePath, enc = 'utf8') {
  return JSON.parse(
    fs.readFileSync(filePath, enc)
  );
}

/*
* Generate and add todo task before committing.
*/
gulp.task('todo', () => gulp.src('src/**/*.js')
  .pipe(todo())
  .pipe(gulp.dest('./')));

/*
* CLean whatever directory is passed in.
*/
function clean(distGlob) {
  return del([distGlob]);
}

/*
* Copy necessary files to the /dist folder for publishing after transpiling.
*/
function copy(fileGlob, destDir) {
  return gulp.src(fileGlob)
    .pipe(gulp.dest(destDir));
}

/*
* Hook transpiling step to gulpSrc passed in.
*/
function transpile(fileGlob, destDir) {
  const babelRC = JSON.parse(JSON.stringify(getFile('./.babelrc')));

  delete babelRC.sourceMaps;

  return gulp.src(fileGlob)
    .pipe(sourcemaps.init())
    .pipe(babel(babelRC))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(destDir));
}

/*
* Count lines. Not implemented.
*/
gulp.task('sloc', () => gulp.src(['src/**/*.js']).pipe(sloc()));

/*
* Linter. Not implemented.
*/
gulp.task('lint', () => {
  // const babelRC = JSON.parse(JSON.stringify(getFile('./.eslintrc')));
  return gulp.src(['src/**/*.js', '!./node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

/* ************************************************************************* */
/* Main Tasks                                                                */
/* ************************************************************************* */

/*
* Transpile src/* to /dist.
*/
gulp.task('transpile', () => transpile('src/**/*.js', 'dist'));
gulp.task('transpile-perf', () => transpile('test/**/*.js', 'perfdist'));

/*
* Clean/create the dist directory.
*/
gulp.task('clean', () => clean('./dist/*'));
gulp.task('clean-perf', () => clean('./perfdist/*'));

/*
* Copy package.json and README.md to new dist directory.
*/
gulp.task('copy', () => (
  copy(['./package.json', './README.md'], 'dist'))
);

gulp.task('copy-files', () => {
  return copy(['./test/fixtures/files/*.json'], 'perfdist/fixtures/files/');
});

gulp.task('copy-perf', () => (
  copy(['./package.json', './README.md'], 'perfdist'))
);

/*
* Bump package.json.
* #These are not exposed in any npm script. Used by release tasks.
*/
function bump(importance, tag = '') {
  return gulp.src('./package.json')
    .pipe(bumpVersion({ type: importance, preid: tag }))
    .pipe(gulp.dest('./'));
}

/*
* Git commands used by release tasks
*/
const gitCB = err => {
  if (err) {
    throw err;
  }
};

// GIT TASKS
const commit = msg => git.commit(msg);
const add = glob => gulp.src(glob).pipe(git.add());
const describe = options => git.exec(options, gitCB);
const push = (to, branch, opts) => git.push(to, branch, opts);

/*
* After bumping, get the new version and create a tag of it, commit the tag and push with tags
*/
gulp.task('git-tag', () => {
  const pkg = getFile('./package.json');
  const version = `v${pkg.version}`;
  const commitMsg = `Release ${version}`;
  const tagMsg = `Version ${pkg.version}`;
  const describeOpts = { args: 'describe --tags --always --abbrev=1 --dirty=-d' };

  return add('./package.json')
    .pipe(commit(commitMsg))
    .pipe(git.tag(version, tagMsg, gitCB))
    .pipe(describe(describeOpts))
    .pipe(push('origin', 'master', { args: '--tags' }));
});

/*
* Pack everything for zip-file bodhishop
*/

// gulp.task('bodhishop', () => )
// gulp.task('zip-bodhishop', () => runSequence(
//   'default',
//   'bodhishop'
// ));
/*
* Release tasks.
*/
gulp.task('prerelease', () => bump('prerelease', 'beta'));
gulp.task('patch', () => bump('patch'));
gulp.task('major', () => bump('major'));
gulp.task('minor', () => bump('minor'));
gulp.task('no-bump', () => gulp.src([]));

gulp.task('perf', () =>
  runSequence('clean-perf',
    ['transpile-perf', 'copy-perf', 'copy-files']
  )
);
/*
* Release task that update package.json according to what the release does
* according to SemVer and transpile the code.
*/
function release(importance) {
  return runSequence(
    'lint',
    'sloc',
    'clean',
    importance,
    'git-tag',
    ['transpile', 'copy']
  );
}

/*
* Core release tasks.
*/
gulp.task('release', () => (release('no-bump')));
gulp.task('release-patch', () => (release('patch')));
gulp.task('release-minor', () => (release('minor')));
gulp.task('release-major', () => (release('major')));
gulp.task('release-prerelease', () => (release('prerelease')));

/*
* Default build for main. No bump.
*/
gulp.task('default', () =>
  runSequence(
    'lint',
    'sloc',
    'clean',
    ['transpile', 'copy']
  )
);


