import test from 'ava';
import fs from 'fs';
import childProcess from 'child_process';
import del from 'del';
import fileManager from '../src/index';

const exec = childProcess.exec;

function testState(t) {
  return {
    testExecuted: 0,
    testDone() {
      this.testExecuted = this.testExecuted += 1;
      const testsDone = this.testExecuted;

      if (testsDone === 4) {
        t.end();
      }
    }
  };
}

function adjustProps(props) {
  if (process.platform === 'linux') {
    return Object.assign({}, props, { statProp: 'atime' });
  }

  return props;
}

test.cb.before(t => {
  del([
    './fixtures/deleteFromGlob/*.json',
    './fixtures/deleteFromNoTimeType/*.json',
    './fixtures/deleteFromWithTimeType/*.json',
    './fixtures/deleteFromMultipleDirs/**/*.json'
  ])
    .then(() => {
      exec('. ../createTestFiles.sh', {}, err => {
        if (err) {
          t.fail(`Failed to create test files ${JSON.stringify(err, null, 4)}`);
        }

        t.end();
      });
    })
    .catch(err => t.fail(`Failed to clean deleteFrom dir ${err}`));
});

// test.after.always(async () => {
//   await del([
//     './fixtures/deleteFromGlob#<{(|.json',
//     './fixtures/deleteFromNoTimeType#<{(|.json',
//     './fixtures/deleteFromWithTimeType#<{(|.json',
//     './fixtures/deleteFromMultipleDirs#<{(||)}>#*json'
//   ]);
// });

test.cb('fileManager().erase({dirGlob}, cb)', t => {
  const opts = adjustProps({
    dirGlob: './fixtures/deleteFromNoTimeType/*',
  });

  const manager = fileManager();

  const expectedPathsErased = [
    './fixtures/deleteFromNoTimeType/oneMonthOld.json',
    './fixtures/deleteFromNoTimeType/oneMonthOldThree.json',
    './fixtures/deleteFromNoTimeType/oneMonthOldTwo.json',
    './fixtures/deleteFromNoTimeType/sixMonthOld.json',
    './fixtures/deleteFromNoTimeType/sixMonthOldTwo.json'
  ];

  const expectedFilesLeft = [
    'oneWeekOld.json'
  ];

  manager.erase(opts, (err, pathsErased) => {
    if (err) {
      t.fail((err));
    }

    t.deepEqual(pathsErased, expectedPathsErased, 'Should return array of all file paths deleted');

    fs.readdir('./fixtures/deleteFromNoTimeType', (error, files) => {
      const filesThatExist = files.filter(file => file[0] !== '.');
      t.deepEqual(filesThatExist, expectedFilesLeft,
        'Should list files that are younger than 1 month');
      t.end();
    });
  });
});

test.cb('fileManager().erase({dirGlob, amountToSubtract}, cb)', t => {
  const opts = adjustProps({
    dirGlob: './fixtures/deleteFromWithTimeType/*',
    amountToSubtract: 60
  });

  const manager = fileManager();

  const expectedPathsErased = [
    './fixtures/deleteFromWithTimeType/sixMonthOld.json',
    './fixtures/deleteFromWithTimeType/sixMonthOldTwo.json'
  ];

  const expectedFilesLeft = [
    'oneMonthOld.json',
    'oneMonthOldThree.json',
    'oneMonthOldTwo.json',
    'oneWeekOld.json'
  ];

  manager.erase(opts, (err, pathsErased) => {
    if (err) {
      t.fail(err);
    }

    t.deepEqual(pathsErased, expectedPathsErased, 'Should return array of all file paths deleted');

    fs.readdir('./fixtures/deleteFromWithTimeType', (error, files) => {
      const filesThatExist = files.filter(file => file[0] !== '.');
      t.deepEqual(filesThatExist, expectedFilesLeft,
        'Should list files that are younger than 2 month');
      t.end();
    });
  });
});

test.cb('fileManager().erase({dirGlob - glob, amountToSubtract}, cb)', t => {
  const opts = adjustProps({
    dirGlob: './fixtures/deleteFromGlob/{sixMonthOld,sixMonthOldTwo}.json',
    amountToSubtract: 60
  });

  const manager = fileManager();

  const expectedPathsErased = [
    './fixtures/deleteFromGlob/sixMonthOld.json',
    './fixtures/deleteFromGlob/sixMonthOldTwo.json'
  ];

  const expectedFilesLeft = [
    'oneMonthOld.json',
    'oneMonthOldThree.json',
    'oneMonthOldTwo.json',
    'oneWeekOld.json',
    'sixMonthOldNope.json'
  ];

  manager.erase(opts, (err, pathsErased) => {
    if (err) {
      t.fail(err);
    }

    t.deepEqual(pathsErased, expectedPathsErased, 'Should return array of all file paths deleted');

    fs.readdir('./fixtures/deleteFromGlob', (error, files) => {
      const filesThatExist = files.filter(file => file[0] !== '.');
      t.deepEqual(filesThatExist, expectedFilesLeft,
        'Should list files that are younger than 2 month');
      t.end();
    });
  });
});

test.cb('fileManager().erase({dirGlob - globArray, amountToSubtract}, cb)', t => {
  t.plan(4);

  const testTracker = testState(t);
  const glob = ['./fixtures/deleteFromMultipleDirs/dirOne/*.json',
    './fixtures/deleteFromMultipleDirs/dirThree/*.json'];
  const opts = adjustProps({
    dirGlob: glob,
    amountToSubtract: 10
  });

  const manager = fileManager();

  const expectedPathsErased = [
    './fixtures/deleteFromMultipleDirs/dirOne/oneWeekOld.json',
    './fixtures/deleteFromMultipleDirs/dirThree/sixMonthOld.json',
    './fixtures/deleteFromMultipleDirs/dirThree/sixMonthOldTwo.json'
  ];

  const expectedFilesLeft = [
    'oneMonthOld.json',
    'oneMonthOldThree.json',
    'oneMonthOldTwo.json'
  ];

  manager.erase(opts, (err, pathsErased) => {
    if (err) {
      t.fail(err);
    }

    t.deepEqual(pathsErased, expectedPathsErased, 'Should return array of all file paths deleted');
    testTracker.testDone();

    fs.readdir('./fixtures/deleteFromMultipleDirs/dirOne', (error, files) => {
      t.is(files.length, 0, 'Should return no files left');
      testTracker.testDone();
    });

    fs.readdir('./fixtures/deleteFromMultipleDirs/dirTwo', (error, files) => {
      const filesThatExist = files.filter(file => file[0] !== '.');

      t.deepEqual(filesThatExist, expectedFilesLeft,
        'Should list files that are younger than 2 month');

      testTracker.testDone();
    });

    fs.readdir('./fixtures/deleteFromMultipleDirs/dirThree', (error, files) => {
      t.is(files.length, 0, 'Should return no files left');
      testTracker.testDone();
    });
  });
});
