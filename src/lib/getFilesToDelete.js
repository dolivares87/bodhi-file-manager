import fs from 'mz/fs';
import glob from 'glob-promise';

export default async function getFilesToDelete(dirGlob, shouldDeleteFn) {
  const filesInDir = await glob(dirGlob, { nodir: true });

  return filesInDir.reduce(async (filePaths, filePath) => {
    const pathsToDelete = await filePaths;

    const fileStat = await fs.stat(filePath);

    if (shouldDeleteFn(fileStat)) {
      pathsToDelete.push(filePath);
    }

    return pathsToDelete;
  }, []);
}
