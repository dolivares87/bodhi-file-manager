import fs from 'mz/fs';

export default function deleteFiles(filesToDelete = []) {
  return filesToDelete.reduce(async (files, path) => {
    const deletedFiles = await files;
    await fs.unlink(path);

    deletedFiles.push(path);

    return deletedFiles;
  }, []);
}
