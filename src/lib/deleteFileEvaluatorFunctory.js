import moment from 'moment';
import getPath from 'lodash.get';

export default function deleteFileEvaluatorFunctory(opts = {}) {
  const { amountToSubtract = 30, typeOfTime = 'days', statProp = 'birthtime' } = opts;
  const olderThanDate = moment()
    .subtract(amountToSubtract, typeOfTime);

  return function shouldDeleteFile(fileStat) {
    return moment(getPath(fileStat, statProp))
      .isBefore(olderThanDate);
  };
}
