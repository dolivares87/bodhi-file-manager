```js
import getFilesToDelete from './lib/getFilesToDelete';
import deleteFiles from './lib/deleteFiles';
import extractTimeOption from './lib/extractTimeOption';
import deleteFileEvaluatorFunctory from './lib/deleteFileEvaluatorFunctory';

```
# `index.js`

## fileManager
```js
const fileManager = {
```
### erase(opt, cb)
opt - { filePath, olderThan } =>
filePath - String - The file directory path where to look for the files to delete
olderThan - String - A AMOUNT TIMETYPE string such as '1 month', '2 months', '1 week'
```js
  async erase(opts, cb) {
    const { filePath, olderThan = '' } = opts;
    const shouldDeleteFileFn = deleteFileEvaluatorFunctory(extractTimeOption(olderThan));

    try {
      const filesToDelete = await getFilesToDelete(filePath, shouldDeleteFileFn);
      const filesDeleted = await deleteFiles(filesToDelete);

      return cb(null, filesDeleted);
    } catch (e) {
      return cb(e);
    }
  }
};

export default function fileManagerFactory() {
  return Object.create(fileManager);
}

```
------------------------
Generated _Tue Aug 23 2016 12:57:07 GMT-0500 (CDT)_ from [&#x24C8; index.js](index.js "View in source")

