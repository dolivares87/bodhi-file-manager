import getFilesToDelete from './lib/getFilesToDelete';
import deleteFiles from './lib/deleteFiles';
import deleteFileEvaluatorFunctory from './lib/deleteFileEvaluatorFunctory';
import arraify from './lib/arrayify';

// # `index.js`

function flattenFilePaths(arrayOfArrays) {
  return arrayOfArrays.reduce((flat, arr) => {
    return flat.concat(...arr);
  }, []);
}

function eraseFilesFunctory(opts = {}) {
  return async function eraseFiles(glob) {
    const shouldDeleteFileFn = deleteFileEvaluatorFunctory(opts);
    const filesToDelete = await getFilesToDelete(glob, shouldDeleteFileFn);

    return await deleteFiles(filesToDelete);
  };
}

// ## fileManager
const fileManager = {
  // ### erase(opt, cb)
  // opt - { dirGlob, olderThan } =>
  //    filePath - String - The file directory path where to look for the files to delete
  //    olderThan - String - A AMOUNT TIMETYPE string such as '1 month', '2 months', '1 week'
  async erase(opts, cb) {
    const { dirGlob, ...otherOpts } = opts;
    const eraseFilesInDir = eraseFilesFunctory(otherOpts);

    try {
      const filesDeleted = await Promise.all(arraify(dirGlob).map(async glob => {
        return await eraseFilesInDir(glob);
      }));

      return cb(null, flattenFilePaths(filesDeleted));
    } catch (e) {
      return cb(e);
    }
  }
};

export default function fileManagerFactory() {
  return Object.create(fileManager);
}
