#!/bin/bash
touch -a -m -t 201608150000 ./fixtures/deleteFromNoTimeType/oneWeekOld.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromNoTimeType/oneMonthOld.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromNoTimeType/oneMonthOldTwo.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromNoTimeType/oneMonthOldThree.json &&
touch -a -m -t 201602220000 ./fixtures/deleteFromNoTimeType/sixMonthOld.json &&
touch -a -m -t 201602220000 ./fixtures/deleteFromNoTimeType/sixMonthOldTwo.json

touch -a -m -t 201608150000 ./fixtures/deleteFromWithTimeType/oneWeekOld.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromWithTimeType/oneMonthOld.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromWithTimeType/oneMonthOldTwo.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromWithTimeType/oneMonthOldThree.json &&
touch -a -m -t 201602220000 ./fixtures/deleteFromWithTimeType/sixMonthOld.json &&
touch -a -m -t 201602220000 ./fixtures/deleteFromWithTimeType/sixMonthOldTwo.json

touch -a -m -t 201608150000 ./fixtures/deleteFromGlob/oneWeekOld.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromGlob/oneMonthOld.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromGlob/oneMonthOldTwo.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromGlob/oneMonthOldThree.json &&
touch -a -m -t 201602220000 ./fixtures/deleteFromGlob/sixMonthOld.json &&
touch -a -m -t 201602220000 ./fixtures/deleteFromGlob/sixMonthOldNope.json &&
touch -a -m -t 201602220000 ./fixtures/deleteFromGlob/sixMonthOldTwo.json

touch -a -m -t 201608150000 ./fixtures/deleteFromMultipleDirs/dirOne/oneWeekOld.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromMultipleDirs/dirTwo/oneMonthOld.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromMultipleDirs/dirTwo/oneMonthOldTwo.json &&
touch -a -m -t 201607220000 ./fixtures/deleteFromMultipleDirs/dirTwo/oneMonthOldThree.json &&
touch -a -m -t 201602220000 ./fixtures/deleteFromMultipleDirs/dirThree/sixMonthOld.json &&
touch -a -m -t 201602220000 ./fixtures/deleteFromMultipleDirs/dirThree/sixMonthOldTwo.json
